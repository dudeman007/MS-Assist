﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;

namespace MS_Assist
{ 
    public partial class MainForm : Form
    {
        // DLL imports for mouse clicks
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(long dwFlags, long dx, long dy, long cButtons, long dwExtraInfo);
        [DllImport("user32.dll")]
        // Global variables/ I hate globals
        static extern bool SetCursorPos(int X, int Y);
        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;
        int minionSenderX, minionSenderY, origX, origY, lootAcceptX, lootAcceptY, intCountOfMinions = 0;
        Form1 frm = new Form1();// About form
        Thread workerThread;// Worker thread for autoclick/infin loop

        // MainForm Start
        public MainForm()
        {
            InitializeComponent();
            stopBtn.Enabled = false;
            startBtn.Enabled = false;
            workerThread = new Thread(new ThreadStart(startAutoClicker));
        }

        // On click show about info form
        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form1 frm = new Form1();
            frm.Show();
        }

        // Get specific click location for MinionSender in-game add-on
        private void getCursorPos()
        {
            MessageBox.Show("Place your cursor on the Minion Sender, you have 5 secs! Please wait for confirmation of saved coords.", "MS Assist");
            System.Threading.Thread.Sleep(5000);// Wait 5 secs for user
            minionSenderX = MousePosition.X;
            minionSenderY = MousePosition.Y;
            msX.Text = minionSenderX.ToString();
            msY.Text = minionSenderY.ToString();
            MessageBox.Show("Minion Sender postition saved.", "MS Assist");
        }

        // Get loot bag accept button location
        private void getAcceptBtnPos()
        {
            MessageBox.Show("Place your cursor over the loot bag accept button, you have 5 secs! Please wait for confirmation of saved coords.", "MS Assist");
            System.Threading.Thread.Sleep(5000);// Wait 5 secs for user
            lootAcceptX = MousePosition.X;
            lootAcceptY = MousePosition.Y;
            laX.Text = lootAcceptX.ToString();
            laY.Text = lootAcceptY.ToString();
            MessageBox.Show("Loot bag accept button postition saved.", "MS Assist");
        }

        // Function for left clicking the minionsender in-game icon
        private void minionClick()
        {
            for (int i = 0; i <= intCountOfMinions; i++)
            {
                SetCursorPos(minionSenderX, minionSenderY);
                mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, minionSenderX, minionSenderY, 0, 0);
            }
        }

        // Function for left mouse click on loot bag accept button.
        private void lootClick()
        {
            for (int i = 0; i <= intCountOfMinions; i++)
            {
                SetCursorPos(lootAcceptX, lootAcceptY);
                mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, lootAcceptX, lootAcceptY, 0, 0);
            }
        }

        // First time use to send minion out/ also used to send minion out after collecting loot
        private void firstTimeUse()
        {
            for (int i = 0; i <= intCountOfMinions; i++)
            {
                SetCursorPos(minionSenderX, minionSenderY);
                mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, minionSenderX, minionSenderY, 0, 0);
            }
        }

        // Convert the user input of # of minions from a string to an interger
        private void txtCountOfMinions_TextChanged(object sender, EventArgs e)
        {
            intCountOfMinions = Convert.ToInt16(textBox1.Text);
        }

        // On click setup button get pos of minionsender icon and loot bag accept button
        private void setupBtn_Click(object sender, EventArgs e)
        {
            getCursorPos();
            getAcceptBtnPos();
            startBtn.Enabled = true;
        }

        // Autoclick function for sending minions and collecting loot. This is the main loop!
        private void startAutoClicker()
        {
            origX = MousePosition.X;
            origY = MousePosition.Y;
                // Check if x and y coords are empty or "0", if empty ask to setup
                if (minionSenderX != 0 & minionSenderY != 0)
                {
                    firstTimeUse();
                    SetCursorPos(origX, origY);
                while (true)
                    {
                        Thread.Sleep(300500);// Sleep workerthread for 5 mins then perfom auto clicks 
                        // Get orig mouse coord
                        origX = MousePosition.X;
                        origY = MousePosition.Y;
                        for (int i = 0; i != intCountOfMinions; i++)
                        {
                            minionClick();
                            Thread.Sleep(2000);
                            lootClick();
                            Thread.Sleep(2000);
                            firstTimeUse();
                            Thread.Sleep(2000);
                        }
                        SetCursorPos(origX, origY);
                    }
                }
                else
                {
                    // Show msg if saved coords are 0 or empty
                    MessageBox.Show("Please setup the cursor click locations.", "MS Assist");
                }
            }

        // On button click start the workerthread and startAutoClick()
        private void startBtn_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != String.Empty)
            {
                intCountOfMinions = Convert.ToInt16(textBox1.Text);
                
                if (!workerThread.IsAlive)
                {
                    workerThread = new Thread(new ThreadStart(startAutoClicker));
                    workerThread.Start();
                    startBtn.Enabled = false;
                    stopBtn.Enabled = true;
                }
                else
                {
                    workerThread.Start();
                    startBtn.Enabled = false;
                    stopBtn.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show("Please enter a number of Minions to send out!", "MS Assist");
            }
        }

        // Dispose of workerthread and close all forms
        private void exitBtn_Click(object sender, EventArgs e)
        {
            if(workerThread.IsAlive) {
                try
                {
                    workerThread.Abort();
                }
                catch (ThreadAbortException)
                {
                    MessageBox.Show("ThreadAbortException!", "Thread Abort Error");
                    throw;
                }
                frm.Close();
                this.Close();
            }
            else
            {
                frm.Close();
                this.Close();
            }    
        }

        // Stop and dispose the workerthread and autoclick funtion
        private void stopBtn_Click(object sender, EventArgs e)
        {        
            stopBtn.Enabled = false;
            workerThread.Abort();
            startBtn.Enabled = true;
        }

        // Setup click locations/same as the button
        private void setupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            getCursorPos();
            getAcceptBtnPos();
            startBtn.Enabled = true;
        }

        // Dispose of workerthread and close all forms/same as the exit button
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (workerThread.IsAlive)
            {
                try
                {
                    workerThread.Abort();
                }
                catch (ThreadAbortException)
                {
                    MessageBox.Show("ThreadAbortException!", "Thread Abort Error");
                    throw;
                }
                frm.Close();
                this.Close();
            }
            else
            {
                frm.Close();
                this.Close();
            }
        }

        // Opens default browser and navigates user to my project site
        private void downloadPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://trumpstestbed.noip.me:91");
        }

        // Opens the instruction text file for the user
        private void instructionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filePath = "instructions.txt";
            Process.Start("notepad", filePath);
        }
    }
}
