﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MS_Assist
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // About form link/ opens browers and navigates user to my project site
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://trumpstestbed.noip.me:91");
        }
    }
}
